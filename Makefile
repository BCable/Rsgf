all: Rpkg

clean:
	rm -f Rsgf_*.tar.gz
	rm -rf Rsgf.Rcheck
	rm -rf build

Rpkg:
	mkdir -p build
	find . -maxdepth 1 \
		! -name . \
		! -name .. \
		! -name build \
		! -name LICENSE.full \
		! -name Makefile \
		! -name .git | \
			xargs -I{} cp -a {} build
	R CMD build build

check:
	R CMD check --as-cran Rsgf_*.tar.gz
